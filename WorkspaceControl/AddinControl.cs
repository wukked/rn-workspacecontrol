﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WorkspaceControl
{
    public partial class AddinControl : UserControl
    {
        private bool inDesignMode;

        public AddinControl()
        {
            InitializeComponent();
        }

        public AddinControl(bool inDesignMode) : this()
        {
            this.inDesignMode = inDesignMode;
        }

    }
}
