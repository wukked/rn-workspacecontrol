﻿/****************************
 * 
 * Written by:  Kevin Wu @ Accenture
 * Date:        9/7/13
 * Purpose:     Adds a workspace control add in
 * 
 ****************************/

using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Windows.Forms;

namespace WorkspaceControl
{
    [AddIn("Workspace Add In Test", Version = "1.0.0.0")]
    /// <summary>
    /// Factory class for addin
    /// </summary>
    public class Factory : IWorkspaceComponentFactory2
    {
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            //returns the in design status bool
            return new Component(inDesignMode);
        }

        //Returns no image
        public System.Drawing.Image Image16
        {
            get;
            set;
        }

        public string Text
        {
            get { return "Workspace Control Addin Test"; }
        }

        public string Tooltip
        {
            get { return "Workspace Control Addin Test Tooltip"; }
        }

        //initialize add in
        public bool Initialize(IGlobalContext context)
        {
            MessageBox.Show( "Initialized" );
            return true;
        }
    }

    /// <summary>
    /// Component class for add in
    /// </summary>
    public partial class Component : IWorkspaceComponent2
    {
        private AddinControl control;
        private bool inDesignMode;

        public Component( bool inDesignMode)
        {
            this.inDesignMode = inDesignMode;
            this.control = new AddinControl(inDesignMode);    
        }

        public bool ReadOnly
        {
            get;
            set;
        }

        //workspace rule action
        public void RuleActionInvoked(string actionName)
        {
            throw new NotImplementedException();
        }

        //workspace rule condition
        public string RuleConditionInvoked(string conditionName)
        {
            throw new NotImplementedException();
        }

        public System.Windows.Forms.Control GetControl()
        {
            return control;
        }
    }
}
